/*
   Copyright (C) 2003 Zentaro Kavanagh
   
   Copyright (C) 2003 Commonwealth Scientific and Industrial Research
   Organisation (CSIRO) Australia

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

   - Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

   - Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

   - Neither the name of CSIRO Australia nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
   PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE ORGANISATION OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#include "config.h"

// These classes are exported from the cpp_lib_cmml.dll
#include <libCMMLTags/C_AnchorTag.h>
#include <libCMMLTags/C_BaseTag.h>
#include <libCMMLTags/C_ClipTag.h>
#include <libCMMLTags/C_ClipTagList.h>
#include <libCMMLTags/C_CMMLDoc.h>
#include <libCMMLTags/C_CMMLPreamble.h>
#include <libCMMLTags/C_CMMLRootTag.h>
#include <libCMMLTags/C_CMMLTag.h>
#include <libCMMLTags/C_DescTag.h>
#include <libCMMLTags/C_HeadTag.h>
#include <libCMMLTags/C_HumReadCMMLTag.h>
#include <libCMMLTags/C_ImageTag.h>
#include <libCMMLTags/C_ImportTag.h>
#include <libCMMLTags/C_ImportTagList.h>
#include <libCMMLTags/C_Int64.h>				//Remove ??
#include <libCMMLTags/C_MappedTag.h>
#include <libCMMLTags/C_MappedTagList.h>
#include <libCMMLTags/C_MetaTag.h>
#include <libCMMLTags/C_MetaTagList.h>
#include <libCMMLTags/C_ParamTag.h>
#include <libCMMLTags/C_ParamTagList.h>
#include <libCMMLTags/C_StreamTag.h>
#include <libCMMLTags/C_TagList.h>
#include <libCMMLTags/C_TextFieldTag.h>
#include <libCMMLTags/C_TitleTag.h>

